
define(['app'], function (app) {

    app.controller('navigatorController', function ($rootScope, $q,  $scope, homeService, $modal, $route, $location) {

        $scope.init = function () {
            
        }

        $scope.edit = function () {
            $rootScope.editEnable =  !$rootScope.editEnable;
        };


        //menu items class activation
        $scope.isActive = function (location) {
            return location === $location.url();
        };

        $scope.$on('$routeChangeStart', function (scope, next, current) {
            if(next.$$route == undefined) return;

            $scope.editButtonDisabled = next.$$route.controller == 'homeController';

        });

        $scope.showLogin = function () {

            $modal.open({
                backdrop: true,
                //windowClass: 'welcomeModel',
                templateUrl: 'app/main/views/partials/login.html',
                controller: 'loginController',
                resolve: { $parentScope: function () { return $scope; } }
            });
        };

        $scope.init();
       
    });

});



