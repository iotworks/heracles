define(['app'], function (app) {
    app.controller('editController', function ($rootScope, $scope, homeService) {
        
        $scope.slides = [];
        $scope.slides.push({ text: 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', image: '/images/business2.jpg' });
        $scope.slides.push({ text: 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', image: '/images/slide3.jpg' });
        $scope.slides.push({ text: 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', image: '/images/people.jpg' });

        var getVisionInfo = function () {
            homeService.getVisionInfo("vision").then(function(visions) {
                $scope.vision1 = _.where(visions.data, {subtype: "vision1"})[0];
                $scope.vision2 = _.where(visions.data, {subtype: "vision2"})[0];
                $scope.vision3 = _.where(visions.data, {subtype: "vision3"})[0];
                $scope.visionTitle = _.where(visions.data, {subtype: "visionTitle"})[0];
            }, function(error) {
                console.log(error);
            });
        };

        var getServiceInfo = function () {
            homeService.getServiceInfo().then(function (services) {
                $scope.service1 = _.where(services.data, {subtype: "service1"})[0];
                $scope.service2 = _.where(services.data, {subtype: "service2"})[0];
                $scope.service3 = _.where(services.data, {subtype: "service3"})[0];
                $scope.serviceTitle = _.where(services.data, {subtype: "serviceTitle"})[0];
            }, function (error) {
                console.log(error);
            });
        };

        var getAboutInfo = function () {
            homeService.getAboutInfo().then(function (abouts) {
                $scope.about1 = _.where(abouts.data, {subtype: "about1"})[0];
                $scope.about2 = _.where(abouts.data, {subtype: "about2"})[0];
                $scope.about3 = _.where(abouts.data, {subtype: "about3"})[0];
                $scope.aboutTitle = _.where(abouts.data, {subtype: "aboutTitle"})[0];
            }, function (error) {
                console.log(error);
            });
        };


        var getSeparatorsInfo = function () {
            homeService.getSeparatorsInfo().then(function (separators) {
                $scope.separator1 = _.where(separators.data, {subtype: "separator1"})[0];
                $scope.separator2 = _.where(separators.data, {subtype: "separator2"})[0];
                $scope.separator3 = _.where(separators.data, {subtype: "separator3"})[0];
            }, function (error) {
                console.log(error);
            });
        };

        var getFooterInfo = function () {
            homeService.getFooterInfo().then(function (footers) {
                $scope.footer = _.where(footers.data, {subtype: "footer"})[0];
            }, function (error) {
                console.log(error);
            });
        };

        var visionInfoListener = $scope.$on('UpdateVision', function (event, editable) {

            homeService.updateVisionInfo(editable).then(function (result) {
                console.log(result[0]);
            }, function (error) {
                console.log(error);
            });
            
        });

        var serviceInfoListener = $scope.$on('UpdateService', function (event, editable) {

            homeService.updateServiceInfo(editable).then(function (result) {
                console.log(result[0]);
            }, function (error) {
                console.log(error);
            });

        });

        var aboutInfoListener = $scope.$on('UpdateAbout', function (event, editable) {

            homeService.updateAboutInfo(editable).then(function (result) {
                console.log(result[0]);
            }, function (error) {
                console.log(error);
            });

        });

        var separatorInfoListener = $scope.$on('UpdateSeparator', function (event, editable) {

            homeService.updateSeparatorInfo(editable).then(function (result) {
                console.log(result[0]);
            }, function (error) {
                console.log(error);
            });

        });

        var footerInfoListener = $scope.$on('UpdateFooter', function (event, editable) {

            homeService.updateFooterInfo(editable).then(function (result) {
                console.log(result[0]);
            }, function (error) {
                console.log(error);
            });

        });
        


        $scope.$on("$destroy", function () {
            visionInfoListener();
            serviceInfoListener();
            aboutInfoListener();
            separatorInfoListener();
            footerInfoListener();
        });

        $scope.init = function () {
            getVisionInfo();
            getServiceInfo();
            getAboutInfo();
            getSeparatorsInfo();
            getFooterInfo();
            $rootScope.isAuthenticated = true;
        };

        $scope.init();

    });   

});


