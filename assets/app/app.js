'use strict';

define(['angular',  'angularRoute','bootstrap', 'angularUI', 'modernizr', 'angular-aloha'], function (angular) {

    return angular.module('Heracles', ['ngRoute', 'ui.bootstrap', 'aloha', 'compile', 'contenteditable','smoothscroll']);

});

