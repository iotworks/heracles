'use strict';
define(['app','angular',
    'main/scripts/services/dependencyResolverFor',
    'main/scripts/controllers/base',
    'main/scripts/controllers/partials/navigator',

    'main/scripts/controllers/singles/home',
    'main/scripts/controllers/singles/edit',
    'main/scripts/services/home',

    'main/scripts/controllers/partials/login'
    ],

    function (app, angular,dependencyResolverFor) {

       app.config(['$routeProvider', function($routeProvider) {

            $routeProvider
                    .when('/home', { templateUrl: 'app/main/views/singles/home.html', controller: "homeController" })
                    .when('/edit', { templateUrl: 'app/main/views/singles/edit.html', controller: "editController", resolve:dependencyResolverFor( [ '../../js/aloha' ]) })
                    .otherwise({  redirectTo: '/home'  });
            }]
        );

        app.run(function ($rootScope, $location, $anchorScroll, $routeParams) {

        });
    }
);